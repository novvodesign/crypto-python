## Requisitos

Para montar o ambiente será necessário ter os seguintes itens abaixo:

- python3
- pip3 (Gerenciador de Bibliotecas)
- simple-crypt (4.1.7)

## Instalação

1) Para começar será necessário instalar o Python 3, porém, verifique se já existe o mesmo instalado no seu computador, caso contrário digite o comando abaixo no seu terminal.

```
sudo apt-get install python3
```

2) Logo após instalar o python, é necessário instalar o gereciador de bibliotecas, o mesmo deve estar na mesma versão na qual foi instalado o python.

```
sudo apt-get install python3-pip
```

3) Em seguida vamos instalar o pacote necessário para funcionar a criptografia.

```
sudo pip3 install 'simple-crypt==4.1.7'
```

## Utilização

Após montar o ambiente, vá até a pasta que foi clonada e execute o seguinte comando:

```
python3 app.py
```

Quando executado o comando acima, será criada duas pastas, sendo elas **inbound** e **outbound**. A pasta **inbound** estará disponível para receber todos os arquivos que serão criptografados e descriptografados, a pasta **outbound** disponibilizará todos os arquivos que foram criptografados e descriptografados no final do processo.

Após colocar os arquivos em seus devidos lugares, seguir os passos que serão descritos conforme o processo de criptografia e descriptografia.

**Obs:** Lembre-se de armazenar sua chave de criptografia.

## Autor

*by Daniel Bruno*

[**_NovvoDesign_**](https://www.novvodesign.com.br/)