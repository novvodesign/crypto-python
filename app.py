import os
from simplecrypt import encrypt, decrypt
from base64 import b64encode, b64decode

#Pastas que serão inseridos e retiratos os arquivos
entry = 'inbound'
exit = 'outbound'

#Cria as pastas
if os.path.exists(entry) == False:
	os.mkdir(entry)

if os.path.exists(exit) == False:
	os.mkdir(exit)

#Coleta informações do usuário
mode = int(input('Escolha as opção abaixo:\n1) Criptografar\n2) Descriptografar\n>'))
code = input('\nInsira a chave de criptografia: ')

#Armazena a quantidade de arquivos modificados
success_files = 0

#Verifica qual o modo que deve ser executado
if mode == 1:

	#Lista os arquivos da pasta
	archives = os.listdir(entry)

	#Realiza a leitura e cópia do arquivo
	for name in archives:
		
		#Abre o arquivo e pega o conteúdo
		archive = open(entry + '/' + name, 'rb')
		content = archive.read()
		archive.close()

		content_crypto = b64encode(encrypt(code, content))

		archive_crypt = open(exit + '/' + name, 'w')
		archive_crypt.write(content_crypto.decode('utf8'))
		archive_crypt.close()

		print('{} criptografado com sucesso!'.format(name))

		success_files += 1

#Verifica qual o modo que deve ser executado
if mode == 2:

	#Lista os arquivos da pasta
	archives = os.listdir(entry)

	#Realiza a leitura e cópia do arquivo
	for name in archives:

		#Abre o arquivo e pega o conteúdo
		archive = open(entry + '/' + name, 'r')
		content = archive.read()
		archive.close()

		content_crypto = decrypt(code, b64decode(content.encode()))

		archive_crypt = open(exit + '/' + name, 'wb')
		archive_crypt.write(content_crypto)
		archive_crypt.close()

		print('{} descriptografado com sucesso!'.format(name))

		success_files += 1

#Verifica se foi digitado algo diferente
if mode != 1 and mode != 2:

	print('Nenhuma função localizada!')

#Finaliza a aplicação e exibe quantos arquivos foram modificados
print('{} Arquivo(os) foram modificados!'.format(success_files))